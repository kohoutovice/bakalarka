import numpy as np
import argparse
import pickle
import os

parser = argparse.ArgumentParser(description='')
parser.add_argument('--persons', action='store', default="/home/rmin/Dokumenty/extracted/person_descs/dlib/dlib.persons", help='save')
parser.add_argument('--save', action='store', default="dlib.agregated", help='save')
parser.add_argument('--tensors', action='store_true',  help='vectors are saved as tensors')

args = parser.parse_args()

persons = pickle.load(open(args.persons,'rb'))

agregated = {}
for person_id in persons:
    print(person_id)
    agregated[person_id] = {}
    for cam in persons[person_id]:
        if args.tensors:
            frames = [x.tolist() for x in persons[person_id][cam]]
        else:
            frames = persons[person_id][cam]

        agregated[person_id][cam] = np.array(frames).mean(axis=0)

pickle.dump(agregated,open(args.save,'wb'))

import numpy as np
import argparse
import pickle
import os
import glob

parser = argparse.ArgumentParser(description='')
parser.add_argument('--files', action='append', default=[], help='save')
parser.add_argument('--save', action='store' , required=True, help='save')

args = parser.parse_args()

merged = {}

for file in args.files:
    l = pickle.load(open(file,'rb'))
    merged.update(l)

pickle.dump(merged,open(args.save,'wb'))

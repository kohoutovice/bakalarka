import numpy as np
import argparse
import pickle
import os
import glob
import torch
import random
'''
input file is pickled dict in structure

file = {
"filename with path": [descriptor]
}

'''
out = {}

parser = argparse.ArgumentParser(description='')
parser.add_argument('--src', action='store', default="/home/rmin/Dokumenty/extracted/descs/BUT_pedestrians/dlib/gait.pic", help='save')
parser.add_argument('--train', action='store' , required=True, help='save')
parser.add_argument('--test', action='store' , required=True, help='save')
parser.add_argument('--test_word', action='store' , default=None, help='save')
parser.add_argument('--train_word', action='store' , default=None, help='save')
parser.add_argument('--sample', action='store' ,default=None,  help='how much percents of data should reserver to test')
args = parser.parse_args()

test_perons =["fit_1077","fit_2393","fit_3436","fit_4356","fit_4920","fit_5292","fit_5504","hus_1008","hus_1310","hus_501","ns1_1640","ns1_292","ns1_4632","ns1_74","ns2_10287","ns2_1757","ns2_2908","ns2_3887","ns2_6041","ns2_8739","fit_1206","fit_2594","fit_3510","fit_4531","fit_5061","fit_5310","fit_5508","hus_1012","hus_136","hus_503","ns1_20","ns1_3028","ns1_4906","ns1_757","ns2_10435","ns2_1771","ns2_2958","ns2_4","ns2_6521","ns2_8985","fit_1555","fit_3053","fit_3518","fit_4789","fit_508","fit_5417","fit_5642","hus_11","hus_1611","hus_673","ns1_2233","ns1_3654","ns1_5001","ns1_770","ns2_10749","ns2_2063","ns2_3564","ns2_4834","ns2_8034","ns2_9191","fit_1612","fit_3346","fit_3768","fit_4900","fit_5129","fit_5426","fit_811","hus_1115","hus_1820","hus_735","ns1_2383","ns1_3859","ns1_5085","ns1_948","ns2_11097","ns2_2117","ns2_3611","ns2_4880","ns2_8088","ns2_9658","fit_1635","fit_3394","fit_4063","fit_4902","fit_5234","fit_5437","fit_866","hus_1266","hus_216","ns1_1626","ns1_2836","ns1_4320","ns1_5705","ns1_989","ns2_1745","ns2_2254","ns2_3618","ns2_5370","ns2_8273","ns2_9814"]

train_persons = ["fit_1006","fit_1494","fit_2207","fit_3374","fit_3765","fit_4636","fit_5047","fit_5472","fit_918","hus_1827","hus_72","ns1_1619","ns1_3271","ns1_4633","ns1_608","ns2_10546","ns2_1229","ns2_2300","ns2_402","ns2_6046","ns2_8925","fit_1013","fit_1498","fit_2224","fit_3393","fit_3807","fit_4652","fit_505","fit_5502","fit_92","hus_1893","hus_766","ns1_1647","ns1_3337","ns1_4697","ns1_611","ns2_10577","ns2_1233","ns2_2310","ns2_4133","ns2_6050","ns2_8928","fit_1014","fit_1531","fit_2248","fit_3395","fit_3815","fit_4689","fit_5069","fit_5507","fit_961","hus_1926","hus_8","ns1_179","ns1_3368","ns1_4700","ns1_6217","ns2_10659","ns2_1276","ns2_2570","ns2_42","ns2_6053","ns2_8938","fit_1054","fit_1543","fit_225","fit_340","fit_3930","fit_469","fit_5095","fit_5552","fit_963","hus_1928","hus_804","ns1_181","ns1_3370","ns1_48","ns1_647","ns2_10788","ns2_1287","ns2_2581","ns2_445","ns2_6066","ns2_9046","fit_1064","fit_1614","fit_2256","fit_3438","fit_3933","fit_4692","fit_5102","fit_5559","hus_1","hus_1934","hus_810","ns1_1834","ns1_3374","ns1_4950","ns1_709","ns2_10803","ns2_1294","ns2_2692","ns2_4784","ns2_6110","ns2_9147","fit_1066","fit_1616","fit_2282","fit_3446","fit_3975","fit_4712","fit_5109","fit_5576","hus_10","hus_1978","hus_814","ns1_1862","ns1_3401","ns1_4992","ns1_712","ns2_10831","ns2_1489","ns2_2694","ns2_4791","ns2_620","ns2_9248","fit_108","fit_1640","fit_2318","fit_3452","fit_4206","fit_4720","fit_5160","fit_5579","hus_1010","hus_1986","hus_837","ns1_1886","ns1_3568","ns1_5004","ns1_793","ns2_10889","ns2_1501","ns2_27","ns2_4856","ns2_6209","ns2_9641","fit_1094","fit_1644","fit_232","fit_3456","fit_4245","fit_4734","fit_5166","fit_5611","hus_1015","hus_2011","hus_840","ns1_1942","ns1_36","ns1_5021","ns1_93","ns2_10956","ns2_1538","ns2_2713","ns2_4858","ns2_6220","ns2_9664","fit_1102","fit_1646","fit_2468","fit_3462","fit_4269","fit_4741","fit_5171","fit_5613","hus_1021","hus_2027","hus_874","ns1_2021","ns1_3613","ns1_5104","ns1_94","ns2_11010","ns2_1577","ns2_288","ns2_4885","ns2_6264","ns2_9672","fit_1142","fit_1688","fit_2515","fit_3473","fit_4325","fit_4742","fit_5183","fit_562","hus_1070","hus_255","hus_879","ns1_2063","ns1_3630","ns1_5121","ns1_949","ns2_11056","ns2_1708","ns2_2895","ns2_4945","ns2_6287","ns2_9695","fit_1199","fit_1701","fit_2597","fit_3512","fit_4331","fit_4744","fit_5186","fit_5643","hus_1107","hus_26","hus_903","ns1_2097","ns1_3674","ns1_532","ns1_997","ns2_11076","ns2_1714","ns2_2912","ns2_4972","ns2_707","ns2_9713","fit_1208","fit_1729","fit_2628","fit_3514","fit_4390","fit_4747","fit_5187","fit_569","hus_1261","hus_288","hus_949","ns1_2116","ns1_368","ns1_5380","ns2_10","ns2_11083","ns2_1728","ns2_2917","ns2_523","ns2_7075","ns2_9721","fit_122","fit_1747","fit_2717","fit_3542","fit_4404","fit_4763","fit_5221","fit_610","hus_1271","hus_309","hus_976","ns1_2143","ns1_3755","ns1_54","ns2_10001","ns2_11153","ns2_1737","ns2_2924","ns2_5278","ns2_7090","ns2_9726","fit_1235","fit_1781","fit_2790","fit_3567","fit_4438","fit_4799","fit_5246","fit_623","hus_1278","hus_339","hus_983","ns1_2147","ns1_3780","ns1_5479","ns2_10046","ns2_11176","ns2_1754","ns2_2938","ns2_5316","ns2_7152","ns2_979","fit_1246","fit_1791","fit_2844","fit_3584","fit_4483","fit_4801","fit_5275","fit_628","hus_1312","hus_361","ns1_1043","ns1_2148","ns1_3789","ns1_551","ns2_10049","ns2_1123","ns2_1770","ns2_2957","ns2_5317","ns2_7160","ns2_9841","fit_1247","fit_1803","fit_2849","fit_3589","fit_4532","fit_4857","fit_5280","fit_647","hus_1357","hus_429","ns1_1045","ns1_2154","ns1_3795","ns1_568","ns2_10051","ns2_11264","ns2_1839","ns2_3099","ns2_5332","ns2_7161","ns2_9844","fit_1254","fit_1808","fit_2851","fit_360","fit_4536","fit_4858","fit_5312","fit_66","hus_1389","hus_454","ns1_1076","ns1_2160","ns1_3826","ns1_5684","ns2_10072","ns2_11326","ns2_1897","ns2_3274","ns2_5379","ns2_7203","ns2_991","fit_1259","fit_1876","fit_289","fit_3631","fit_4537","fit_4863","fit_5315","fit_685","hus_1514","hus_484","ns1_1109","ns1_2308","ns1_3846","ns1_5690","ns2_1008","ns2_1156","ns2_1927","ns2_3301","ns2_5418","ns2_7206","ns2_992","fit_1277","fit_1911","fit_3024","fit_3664","fit_4552","fit_4894","fit_5327","fit_715","hus_1531","hus_493","ns1_1114","ns1_2817","ns1_3871","ns1_5697","ns2_10080","ns2_11614","ns2_1945","ns2_3320","ns2_5419","ns2_7733","ns2_9997","fit_128","fit_1969","fit_3045","fit_3666","fit_4555","fit_4906","fit_5348","fit_728","hus_1556","hus_500","ns1_118","ns1_2844","ns1_3877","ns1_5740","ns2_10106","ns2_11817","ns2_2017","ns2_3566","ns2_5442","ns2_7771","fit_1291","fit_1980","fit_3058","fit_3690","fit_458","fit_4930","fit_5358","fit_795","hus_1559","hus_569","ns1_1293","ns1_291","ns1_397","ns1_5765","ns2_10108","ns2_11823","ns2_2021","ns2_3606","ns2_5757","ns2_7855","fit_1295","fit_1989","fit_3095","fit_3710","fit_4581","fit_4931","fit_5375","fit_814","hus_1686","hus_664","ns1_1300","ns1_308","ns1_4108","ns1_5767","ns2_10114","ns2_1193","ns2_2047","ns2_3641","ns2_5782","ns2_7995","fit_1302","fit_2053","fit_3115","fit_3719","fit_4592","fit_494","fit_5397","fit_822","hus_1725","hus_668","ns1_1328","ns1_310","ns1_4185","ns1_5843","ns2_1028","ns2_12042","ns2_2070","ns2_3822","ns2_580","ns2_8218","fit_1309","fit_2085","fit_3126","fit_3724","fit_4595","fit_4942","fit_5412","fit_872","hus_1757","hus_682","ns1_1372","ns1_312","ns1_4470","ns1_5890","ns2_1030","ns2_12070","ns2_2140","ns2_3825","ns2_5954","ns2_8342","fit_1339","fit_2117","fit_3265","fit_3732","fit_4596","fit_4951","fit_5423","fit_893","hus_1770","hus_685","ns1_142","ns1_3138","ns1_4548","ns1_5900","ns2_10333","ns2_12088","ns2_2274","ns2_3826","ns2_5978","ns2_8344","fit_1367","fit_2174","fit_3331","fit_3750","fit_4603","fit_5024","fit_5434","fit_910","hus_1771","hus_7","ns1_1599","ns1_317","ns1_460","ns1_5995","ns2_10372","ns2_12138","ns2_2284","ns2_3882","ns2_6","ns2_8869","fit_1463","fit_2183","fit_334","fit_3757","fit_4606","fit_5027","fit_5435","fit_911","hus_1809","hus_718","ns1_16","ns1_3175","ns1_462","ns1_604","ns2_10443","ns2_1224","ns2_2293","ns2_3893","ns2_6016","ns2_8889"]

test = {}
train = {}
if True:
    files = pickle.load(open(args.src,'rb'))
    keys = list(files.keys())
    for file in keys:
        person_id = file.split("/")[-4]
        print(person_id)

        if person_id in test_perons:
            test[file] = torch.tensor(files[file])

        if person_id in train_persons:
            train[file] = torch.tensor(files[file])

        del files[file]

elif args.test_word and args.train_word:
    files = pickle.load(open(args.src,'rb'))
    keys = list(files.keys())
    for file in keys:
        print(file)
        if args.test_word:
            if args.test_word in file:
                test[file] = torch.tensor(files[file])

        if args.train_word:
            if args.train_word in file:
                train[file] = torch.tensor(files[file])

        del files[file]


elif args.sample:
    persons = pickle.load(open(args.src,'rb'))
    count = int(len(persons)*float(args.sample))
    train_keys = random.sample(list(persons.keys()),count)
    test_keys = [x for x in list(persons.keys()) if x not in train_keys]

    for t in train_keys:
        for s in test_keys:
            if t == s:
                print("=")

    new_train = {}
    new_test = {}

    for k in train_keys:
        train[k] = persons[k]

    for k in test_keys:
        test[k] = persons[k]



if args.test:
    pickle.dump(test,open(args.test,'wb'))

if args.train:
    pickle.dump(train,open(args.train,'wb'))

import matplotlib
from matplotlib import pyplot as plt
import argparse
import os
import shutil
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
import torch.backends.cudnn as cudnn
from tripletnet import Tripletnet

import numpy as np
from tripletDataset import *
from network import *

from sklearn import metrics
from sklearn.metrics import roc_curve, auc

import argparse


def train(train_loader, tnet, criterion, optimizer, epoch):
    # switch to train mode
    tnet.train()

    for batch_idx, (anchor,positive,negative) in enumerate(train_loader):

        # compute output
        dista, distb, embedded_x, embedded_y, embedded_z = tnet(anchor,positive,negative)

        loss_triplet = criterion(embedded_x, embedded_y, embedded_z)
        loss_embedd = embedded_x.norm(2) + embedded_y.norm(2) + embedded_z.norm(2)
        loss = loss_triplet + 0.001 * loss_embedd

        # compute gradient and do optimizer step
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()

        print('Train Epoch: {} [{}/{}]\t'
              'Loss: {:.4f} ({:.4f}) \t'.format(
            epoch, batch_idx * len(anchor), len(train_loader.dataset),
            loss, loss_triplet),end=' ')

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

@static_vars(tc=1)
def plotroc(fpr,tpr):
    plt.clf()
    plt.plot(fpr, tpr, lw=2, label="net")
    plt.legend(loc="lower right")
    plt.pause(0.05)
    #plt.savefig("figure" + str(plotroc.tc) + ".jpg")
    plotroc.tc = plotroc.tc + 1




@static_vars(tpr={})
@static_vars(fpr={})
@static_vars(tc=1)
def test(test_loader, tnet, criterion):
    # switch to evaluation mode
    tnet.eval()
    for batch_idx, (anchor,positive,negative) in enumerate(test_loader):
        # compute output
        dista, distb, em1, em2, em3 = tnet(anchor,positive,negative)
        test_loss =  criterion(em1, em2, em3).data[0]

        pos_labels = torch.tensor(np.full((1, len(distb)), 1).squeeze())
        neg_labels = torch.tensor(np.full((1, len(dista)), 0).squeeze())

        labels = torch.cat((pos_labels,neg_labels),dim=0)
        dists = torch.cat((distb,dista),dim=0)

        fpr, tpr, _ = roc_curve(np.array(labels.tolist()),np.array(dists.tolist()))
        roc_auc = auc(fpr, tpr)
        plotroc(fpr,tpr)

        print('Test set: loss: {:.4f}, AUC: {:.2f}%'.format(test_loss, roc_auc))
    return roc_auc


if __name__ == '__main__':
    plt.figure()
    plt.show(False)
    plt.draw()
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title('Receiver operating characteristic example')
    plt.legend(loc="lower right")

    parser = argparse.ArgumentParser()
    parser.add_argument("--train", action="store",default="/home/rmin/Dokumenty/extracted/agregated/fusion.agregated.triplets", type=str, help="train triplets file")
    parser.add_argument("--test", action="store",default="/home/rmin/Dokumenty/extracted/agregated/fusion.agregated.triplets", type=str, help="test triplets file")
    parser.add_argument("--weights", action="store",type=str, help="load torch")
    parser.add_argument("--batch", action="store",type=int,default=500, help="load torch")
    parser.add_argument("--gf", action="store_true", help="just gate and face")
    parser.add_argument("--model", action="store",required=True, type=str, help="model to use")

    args = parser.parse_args()



    if "pg" in args.model:
        train_loader = torch.utils.data.DataLoader(
            TripletNetworkDatasetUnifiedPG(data=args.train,drop=False),
            batch_size=args.batch, shuffle=True)
        test_loader = torch.utils.data.DataLoader(
            TripletNetworkDatasetUnifiedPG(data=args.test,drop=False),
            batch_size=args.batch, shuffle=False)
    else:
        train_loader = torch.utils.data.DataLoader(
            TripletNetworkDatasetUnified(data=args.train,drop=True),
            batch_size=args.batch, shuffle=True)
        test_loader = torch.utils.data.DataLoader(
            TripletNetworkDatasetUnified(data=args.test,drop=False),
            batch_size=args.batch, shuffle=False)


    models = {
    "cat": FusionCat(),
    "sum": FusionSum(),
    "simple": FusionSimple(),
    "pgcat": PGFusionCat(),
    "pgsum": PGFusionSum(),
    "pgsimple": PGFusionSimple(),
    }

    model = models[args.model]
    tnet = Tripletnet(model)

    criterion = nn.TripletMarginLoss(margin=1.0, p=2)
    optimizer = optim.Adam(tnet.parameters(), lr=0.001)

    if args.weights:
        load_checkpoint(tnet, optimizer, args.weights)

    n_parameters = sum([p.data.nelement() for p in tnet.parameters()])
    print('  + Number of params: {}'.format(n_parameters))

    for epoch in range(0, 50000 + 1):
        # train for one epoch
        train(train_loader, tnet, criterion, optimizer, epoch)
        # evaluate on validation set
        test(test_loader, tnet, criterion)
        state = {'epoch': epoch + 1, 'state_dict': tnet.state_dict(), 'optimizer': optimizer.state_dict() }
        torch.save(state, "fusion.torch." + str(epoch))

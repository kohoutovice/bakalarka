import os
import sys
import time
import datetime
import os.path as osp
import numpy as np

import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.optim import lr_scheduler

from PIL import Image
import os.path as osp

from args import argument_parser, image_dataset_kwargs, optimizer_kwargs
from torchreid.data_manager import ImageDataManager
from torchreid import models
from torchreid.losses import CrossEntropyLoss, DeepSupervision
from torchreid.utils.iotools import save_checkpoint, check_isfile
from torchreid.utils.avgmeter import AverageMeter
from torchreid.utils.loggers import Logger, RankLogger
from torchreid.utils.torchtools import count_num_param, open_all_layers, open_specified_layers, accuracy, load_pretrained_weights
from torchreid.utils.reidtools import visualize_ranked_results
from torchreid.utils.generaltools import set_random_seed
from torchreid.eval_metrics import evaluate
from torchreid.optimizers import init_optimizer

from torchvision.transforms import *
import argparse
import glob
import pickle

# global variables
parser = argparse.ArgumentParser()

# ************************************************************
# Datasets (general)
# ************************************************************
parser.add_argument('--height', type=int, default=256,
                    help='height of an image')
parser.add_argument('--width', type=int, default=128,
                    help='width of an image')
parser.add_argument('--glob', type=str, default="",
                    help='folder of dataset')

parser.add_argument('--start', type=int, default=None,
                    help='start index')
parser.add_argument('--stop', type=int, default=None,
                    help='end index')

parser.add_argument('--file', type=str, default="out.pic",
                    help='file')

# ************************************************************
# Architecture
# ************************************************************
parser.add_argument('-a', '--arch', type=str, default='resnet50')

# ************************************************************
# Test settings
# ************************************************************
parser.add_argument('--load-weights', type=str, default='',
                    help='load pretrained weights but ignore layers that don\'t match in size')

# ************************************************************
# Miscs
# ************************************************************
parser.add_argument('--use-cpu', action='store_true',
                    help='use cpu')
parser.add_argument('--gpu-devices', default='0', type=str,
                    help='gpu device ids for CUDA_VISIBLE_DEVICES')
parser.add_argument('--use-avai-gpus', action='store_true',
                    help='use available gpus instead of specified devices (useful when using managed clusters)')

#parser = argument_parser()
args = parser.parse_args()

def read_image(img_path):
    """Keep reading image until succeed.
    This can avoid IOError incurred by heavy IO process."""
    got_img = False
    if not osp.exists(img_path):
        raise IOError('{} does not exist'.format(img_path))
    img = Image.open(img_path).convert('RGB')
    return img

def main():
    global args
    if not args.use_avai_gpus: os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu_devices
    use_gpu = torch.cuda.is_available()
    if args.use_cpu: use_gpu = False
    print('==========\nArgs:{}\n=========='.format(args))

    if use_gpu:
        print('Currently using GPU {}'.format(args.gpu_devices))
        cudnn.benchmark = True
    else:
        print('Currently using CPU')


    print('Initializing model: {}'.format(args.arch))
    model = models.init_model(name=args.arch, num_classes=2, loss={'xent'}, use_gpu=use_gpu,pretrained="")
    print('Model size: {:.3f} M'.format(count_num_param(model)))

    if args.load_weights and check_isfile(args.load_weights):
        load_pretrained_weights(model, args.load_weights)

    imagenet_mean = [0.485, 0.456, 0.406]
    imagenet_std = [0.229, 0.224, 0.225]
    # build test transformations
    transform = Compose([
            #Resize((160, 64)),
            Resize((args.height, args.width)),
        ToTensor(),
        Normalize(mean=imagenet_mean, std=imagenet_std),
    ])

    model = nn.DataParallel(model).cuda() if use_gpu else model
    ret = {}

    fs = glob.glob(args.glob)
    print(len(fs))
    count = len(fs)
    for i, ifile in enumerate(fs):
        if args.start:
            if i < args.start:
                print("Skipped",i,"/",count,ifile)
                continue
        print(i,"/",len(fs),ifile)

        try:
            a = read_image(ifile)
        except Exception as e:
            continue
        a = transform(a).unsqueeze(0)

        desc = get_desc(model,a,use_gpu)
        ret[ifile] = desc.squeeze()
        if args.stop:
            if i >= args.stop:
                break
                
        if i % 10000 == 0:
            pickle.dump(ret,open(args.file,'wb'))


    pickle.dump(ret,open(args.file,'wb'))

def get_desc(model, imgs, use_gpu):
    model.eval()
    features = None
    with torch.no_grad():
        if use_gpu: imgs = imgs.cuda()

        features = model(imgs)
        features = features.data.cpu()
        qf = features

        #print('Extracted features for query set, obtained {}-by-{} matrix'.format(qf.size(0), qf.size(1)))


    return features

if __name__ == '__main__':
    main()

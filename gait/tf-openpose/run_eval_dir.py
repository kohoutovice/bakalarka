import argparse
import logging
import sys
import time
import glob

from tf_pose import common
import cv2
import numpy as np
from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path, model_wh
import pickle

logger = logging.getLogger('TfPoseEstimator')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


def get_hip(pose):
    try:
        return (pose.body_parts[8].x, pose.body_parts[8].y)
    except Exception as e:
        pass
    try:
        return (pose.body_parts[11].x, pose.body_parts[11].y)
    except Exception as e:
        pass
    raise Exception("Nohip")


def pose_to_vector(pose):
    neck = (pose.body_parts[1].x, pose.body_parts[1].y)
    neck = np.array([neck[0],neck[1]],dtype='float32')
    hip = get_hip(pose)
    hip = np.array([hip[0],hip[1]],dtype='float32')
    body_norm = np.linalg.norm(neck-hip)
    ret = []

    for i in range(common.CocoPart.Background.value):
        if i not in pose.body_parts.keys():
            ret.append([0,0])
            continue

        vec = np.array([pose.body_parts[i].x,pose.body_parts[i].y],dtype='float32')
        vec = vec-neck
        vec = vec/body_norm
        #vec = (vec * 1000)+np.array([100,100])
        ret.append(vec.tolist())

    return ret

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='tf-pose-estimation run')
    parser.add_argument('--glob', type=str, default='./images/')
    parser.add_argument('--model', type=str, default='cmu', help='cmu / mobilenet_thin')
    parser.add_argument('--file', type=str, default='out.pic', help='pickled file output')
    parser.add_argument('--ui', action="store_true",  help='pickled file output')

    parser.add_argument('--resize', type=str, default='128x256',
                        help='if provided, resize images before they are processed. default=0x0, Recommends : 432x368 or 656x368 or 1312x736 ')
    parser.add_argument('--resize-out-ratio', type=float, default=4.0,
                        help='if provided, resize heatmaps before they are post-processed. default=1.0')

    args = parser.parse_args()

    w, h = model_wh(args.resize)
    e = TfPoseEstimator(get_graph_path(args.model), target_size=(w, h))


    all_humans = {}
    files = glob.glob(args.glob)
    files.sort()
    files_count = len(files)
    for i, file in enumerate(files):
        try:
            image = cv2.imread(file)
        except Exception as ex:
            print("cant load image",str(ex))
            continue
        image = cv2.resize(image,(128,256))
        t = time.time()
        humans = e.inference(image, resize_to_default=(w > 0 and h > 0), upsample_size=args.resize_out_ratio)
        elapsed = time.time() - t

        print('inference image: {} in {} seconds. of {}'.format(i, elapsed, int(files_count)),file)


        if args.ui:
            image = TfPoseEstimator.draw_humans(image, humans, imgcopy=False)
            #image = cv2.resize(image, (128,256))                    # Resize image
            cv2.imshow('tf-pose-estimation result', image)
            cv2.waitKey(0)

        if len(humans)>0:

            try:
                all_humans[file] = pose_to_vector(humans[0])
            except Exception as ex:
                print("Incomplete keypoints",str(ex))
                continue

        if i % 10000 == 0:
            pickle.dump(all_humans,open(args.file,'wb'))



    pickle.dump(all_humans,open(args.file,'wb'))

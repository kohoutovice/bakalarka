import argparse
import logging
import sys
import time
import glob

from tf_pose import common
import cv2
import numpy as np
from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path, model_wh
import pickle

logger = logging.getLogger('TfPoseEstimator')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='tf-pose-estimation run')
    parser.add_argument('--video', type=str, default='')
    parser.add_argument('--pose', type=str, default='')

    parser.add_argument('--model', type=str, default='cmu', help='cmu / mobilenet_thin')
    parser.add_argument('--file', type=str, default='out.pic', help='pickled file output')
    parser.add_argument('--ui', type=bool, default=False, help='pickled file output')

    parser.add_argument('--resize', type=str, default='432x368',
                        help='if provided, resize images before they are processed. default=0x0, Recommends : 432x368 or 656x368 or 1312x736 ')
    parser.add_argument('--resize-out-ratio', type=float, default=4.0,
                        help='if provided, resize heatmaps before they are post-processed. default=1.0')

    args = parser.parse_args()

    w, h = model_wh(args.resize)


    cap = cv2.VideoCapture(args.video)
    if cap.isOpened() is False:
        print("Error opening video stream or file")

    poses = pickle.load(open(args.pose,'rb'))
    for pose in poses:
        if cap.isOpened():
            ret_val, image = cap.read()

            if image is None:
                logger.error('Image can not be read, path=%d' % i)
                break

            image = TfPoseEstimator.draw_humans(image, pose, imgcopy=False)


            if args.ui:
                image = cv2.resize(image, (1920,1080))                    # Resize image
                cv2.imshow('tf-pose-estimation result', image)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break

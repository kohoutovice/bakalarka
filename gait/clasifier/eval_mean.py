import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import numpy as np
import argparse
import pickle
import os
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
import glob
from network import *


parser = argparse.ArgumentParser(description='')
parser.add_argument('--track_glob', action='store', default=None, help='save')
parser.add_argument('--weights', action='store', default=None, help='save')
parser.add_argument('--save', action='store', default="gait.agregated", help='save')
args = parser.parse_args()

# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


model = Evalnet().to(device)
optimizer = torch.optim.Adam(model.parameters())

if args.weights:
    load_checkpoint(model, optimizer,  args.weights)


track_folders = list(glob.glob(args.track_glob))
ret = {}
count = len(track_folders)

for i, track_folder in enumerate(track_folders):
    f = track_folder.rstrip("/") + "/*pose"
    poses = list(glob.glob(f))
    poses.sort()
    person_id = f.split("/")[-4]
    cam_id = f.split("/")[-3]


    descs = []
    track = []
    for j,pose in enumerate(poses):
        p = pickle.load(open(pose,'rb'))
        points = []
        for point in p:
            points.extend(point)
        track.append(points)
        if (len(track) % 75 == 0 and j != 0) or j == len(poses)-1:
            if len(track) > 70:
                d = model(torch.tensor([track]),1)
                descs.append(d.squeeze().tolist())
                track = []

    if len(descs) > 0:

        desc = torch.tensor(descs).mean(0)
        
        if person_id not  in ret:
            ret[person_id] = {}
        ret[person_id][cam_id] = desc

        print(i,"/",count,f)


pickle.dump(ret, open(args.save,'wb'))

import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import numpy as np
import argparse
import pickle
import os
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, auc
import torch.nn.functional as F

# Device configuration
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, in_planes, planes, stride=1):
        super(BasicBlock, self).__init__()
        self.conv1 = nn.Conv1d(in_planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn1 = nn.BatchNorm1d(planes)
        self.conv2 = nn.Conv1d(planes, planes, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn2 = nn.BatchNorm1d(planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv1d(in_planes, self.expansion*planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm1d(self.expansion*planes)
            )

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        out = self.bn2(self.conv2(out))
        out += self.shortcut(x)
        out = F.relu(out)
        return out

class Bottleneck(nn.Module):
    expansion = 4

    def __init__(self, in_planes, planes, stride=1):
        super(Bottleneck, self).__init__()
        self.conv1 = nn.Conv1d(in_planes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm1d(planes)
        self.conv2 = nn.Conv1d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm1d(planes)
        self.conv3 = nn.Conv1d(planes, self.expansion*planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm1d(self.expansion*planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv1d(in_planes, self.expansion*planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm1d(self.expansion*planes)
            )

    def forward(self, x):
        out = F.relu(self.bn1(self.conv1(x)))
        out = F.relu(self.bn2(self.conv2(out)))
        out = self.bn3(self.conv3(out))
        out += self.shortcut(x)
        out = F.relu(out)
        return out

class ResNet(nn.Module):
    def __init__(self, block, num_blocks, num_classes=10):
        super(ResNet, self).__init__()
        self.in_planes = 64

        self.conv1 = nn.Conv1d(36, 64, kernel_size=3, stride=1, padding=1, bias=False)
        self.bn1 = nn.BatchNorm1d(64)
        self.layer1 = self._make_layer(block, 64, num_blocks[0], stride=1)
        self.layer2 = self._make_layer(block, 128, num_blocks[1], stride=2)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], stride=2)
        self.layer4 = self._make_layer(block, 512, num_blocks[3], stride=2)
        self.linear = nn.Linear(1024*block.expansion, num_classes)

    def _make_layer(self, block, planes, num_blocks, stride):
        strides = [stride] + [1]*(num_blocks-1)
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    def forward(self, x,l):
        x = x.transpose(dim0=1,dim1=2)
        out = F.relu(self.bn1(self.conv1(x)))
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.layer4(out)
        out = F.avg_pool1d(out, 4)
        out = out.view(out.size(0), -1)
        out = self.linear(out)
        return out


def ResNet18():
    return ResNet(BasicBlock, [2,2,2,2])


class AttentionModule(nn.Module):
    """docstring for AttentionModule."""
    def __init__(self, input_size):
        super(AttentionModule, self).__init__()
        self.input_size = input_size
        self.weight0 = torch.randn(input_size,1)
        self.q0 = torch.nn.Parameter(self.weight0)
        self.fc = nn.Linear(input_size,input_size)

    def forward(self,X):
        o = torch.matmul(X,self.q0)
        #print("o = torch.matmul(X,self.q0)",o.shape)
        o = F.softmax(o, dim=1)
        #print("o = F.softmax(o, dim=1)",o.shape)
        o = X*o
        #print("o = X*o",o.shape)
        o = torch.tanh(self.fc(o))
        #print("o = torch.tanh(self.fc(o))   ",o.shape)
        o = o*X
        #print("o = o*X",o.shape)
        o = o.sum(2)
        #print("o = o.sum(2)",o.shape)
        o = F.softmax(o, dim=1)
        #print("o = F.softmax(o, dim=1)",o.shape)
        o = o.unsqueeze(1).transpose(dim0=1,dim1=2)
        #print("o = o.unsqueeze(1).transpose(dim0=1,dim1=2)",o.shape)
        o = X*o
        #print("o = X*o",o.shape)
        o = o.sum(1)
        #print("o = o.sum(2)",o.shape)
        #exit(0)
        return o

# Recurrent neural network (many-to-one)
class RNNATM(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers):
        super(RNNATM, self).__init__()
        self.hidden_size = 64
        self.num_layers = 2

        #self.gru = nn.GRU(input_size, hidden_size, num_layers, batch_first=True,bidirectional=True)
        self.lstm = nn.LSTM(input_size, self.hidden_size, self.num_layers, batch_first=True,bidirectional=True,dropout=0.05)
        self.atm = AttentionModule(self.hidden_size*2)

    def forward(self, x, X_lengths):
        # Set initial hidden and cell states
        h0 = torch.zeros(self.num_layers*2, x.size(0), self.hidden_size).to(device)
        c0 = torch.zeros(self.num_layers*2, x.size(0), self.hidden_size).to(device)

        X_lengths = np.full((x.shape[0],),x.shape[1]).tolist()

        X = torch.nn.utils.rnn.pack_padded_sequence(x, X_lengths, batch_first=True)
        # Forward propagate
        #X, h_0 = self.gru(X, h0)  # out: tensor of shape (batch_size, seq_length, hidden_size)
        X,(h_0, c_0) = self.lstm(X, (h0, c0))
        X, input_sizes = torch.nn.utils.rnn.pad_packed_sequence(X, batch_first=True)
        o = self.atm(X)

        return o

# Recurrent neural network (many-to-one)
class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers):
        super(RNN, self).__init__()
        self.hidden_size = 64
        self.num_layers = 2

        #self.gru = nn.GRU(input_size, hidden_size, num_layers, batch_first=True,bidirectional=True)
        self.lstm = nn.LSTM(input_size, self.hidden_size, self.num_layers, batch_first=True,bidirectional=True,dropout=0.05)

    def forward(self, x, X_lengths):
        # Set initial hidden and cell states
        h0 = torch.zeros(self.num_layers*2, x.size(0), self.hidden_size).to(device)
        c0 = torch.zeros(self.num_layers*2, x.size(0), self.hidden_size).to(device)

        X_lengths = np.full((x.shape[0],),x.shape[1]).tolist()

        X = torch.nn.utils.rnn.pack_padded_sequence(x, X_lengths, batch_first=True)
        # Forward propagate
        #X, h_0 = self.gru(X, h0)  # out: tensor of shape (batch_size, seq_length, hidden_size)
        X,(h_0, c_0) = self.lstm(X, (h0, c0))
        X, input_sizes = torch.nn.utils.rnn.pad_packed_sequence(X, batch_first=True)
        o = X.mean(1)
        return o



# Recurrent neural network (many-to-one)
class CNNATM(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers):
        super(CNNATM, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv1d(36, 72, kernel_size=10, stride=1, padding=2),
            nn.BatchNorm1d(72),
            nn.ReLU(),
            nn.AvgPool1d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv1d(72, 144, kernel_size=5, stride=1, padding=0),
            nn.BatchNorm1d(144),
            nn.ReLU())

        self.atm = AttentionModule(144)

    def forward(self, x, X_lengths):
        x = x.transpose(dim0=1,dim1=2)
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.transpose(dim0=2,dim1=1)
        out = self.atm(out)
        return out


# Recurrent neural network (many-to-one)
class CNN(nn.Module):
    def __init__(self, input_size=None, hidden_size=None, num_layers=None):
        super(CNN, self).__init__()
        self.layer1 = nn.Sequential(
            nn.Conv1d(36, 72, kernel_size=10, stride=1, padding=2),
            nn.BatchNorm1d(72),
            nn.ReLU(),
            nn.AvgPool1d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv1d(72, 144, kernel_size=5, stride=1, padding=0),
            nn.BatchNorm1d(144),
            nn.ReLU())

    def forward(self, x, X_lengths):
        x = x.transpose(dim0=1,dim1=2)
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.transpose(dim0=2,dim1=1)
        out = out.mean(1)
        return out

# Recurrent neural network (many-to-one)
class CNNRNN(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers):
        super(CNNRNN, self).__init__()
        hidden_size = 128
        num_layers = 1
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.layer1 = nn.Sequential(
            nn.Conv1d(36, 72, kernel_size=10, stride=1, padding=2),
            nn.BatchNorm1d(72),
            nn.ReLU(),
            nn.AvgPool1d(kernel_size=2, stride=2))
        self.layer2 = nn.Sequential(
            nn.Conv1d(72, 144, kernel_size=5, stride=1, padding=0),
            nn.BatchNorm1d(144),
            nn.ReLU())

        self.gru = nn.GRU(144, hidden_size, num_layers, batch_first=True,bidirectional=True)

    def forward(self, x, X_lengths):
        x = x.transpose(dim0=1,dim1=2)
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.transpose(dim0=2,dim1=1)

        h0 = torch.zeros(self.num_layers*2, x.size(0), self.hidden_size).to(device)
        X_lengths = np.full((out.shape[0],),out.shape[1]).tolist()
        out = torch.nn.utils.rnn.pack_padded_sequence(out, X_lengths, batch_first=True)
        out, h0 = self.gru(out, h0)
        out, input_sizes = torch.nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
        return out.mean(1)

# Recurrent neural network (many-to-one)
class CNNRNNATM(nn.Module):
    def __init__(self, input_size, hidden_size, num_layers):
        super(CNNRNNATM, self).__init__()
        hidden_size = 64
        num_layers = 2
        self.hidden_size = hidden_size
        self.num_layers = num_layers
        self.layer1 = nn.Sequential(
            nn.Conv1d(36, 36, kernel_size=6, stride=3, padding=3),
            nn.BatchNorm1d(36)
        )
        self.layer2 = nn.Sequential(
            nn.Conv1d(36, 36, kernel_size=2, stride=1, padding=1),
            nn.BatchNorm1d(36)
        )
        self.gru = nn.GRU(36, hidden_size, num_layers, batch_first=True,bidirectional=True)
        self.atm = AttentionModule(hidden_size*2)

    def forward(self, x, X_lengths):
        x = x.transpose(dim0=1,dim1=2)
        out = self.layer1(x)
        out = self.layer2(out)
        out = out.transpose(dim0=1,dim1=2)
        h0 = torch.zeros(self.num_layers*2, x.size(0), self.hidden_size).to(device)
        X_lengths = np.full((out.shape[0],),out.shape[1]).tolist()
        out = torch.nn.utils.rnn.pack_padded_sequence(out, X_lengths, batch_first=True)
        out, h0 = self.gru(out, h0)
        out, input_sizes = torch.nn.utils.rnn.pad_packed_sequence(out, batch_first=True)
        out = self.atm(out)
        return out

class Tripletnet(nn.Module):
    def __init__(self, embeddingnet):
        super(Tripletnet, self).__init__()
        self.embeddingnet = embeddingnet

    def forward(self, anchor, positive, negative,lenghts):
        embedded_x = self.embeddingnet(anchor,lenghts)
        embedded_y = self.embeddingnet(positive,lenghts)
        embedded_z = self.embeddingnet(negative,lenghts)
        dist_a = F.pairwise_distance(embedded_x, embedded_y, 2)
        dist_b = F.pairwise_distance(embedded_x, embedded_z, 2)
        return dist_a, dist_b, embedded_x, embedded_y, embedded_z


class SoftmaxRNN(nn.Module):
    """docstring for SoftmaxRNN."""
    def __init__(self,hidden_size, num_classes,net):
        super(SoftmaxRNN, self).__init__()
        self.net = net
        self.fc = nn.Linear(hidden_size, num_classes)
        self.soft = torch.nn.Softmax(dim=1)

    def forward(self, x, X_lengths):
        out = self.net(x,X_lengths)
        out = self.fc(out)
        #out = self.soft(out)
        return out

def load_checkpoint(model, optimizer, filename='checkpoint.pth.tar'):
    # Note: Input model & optimizer should be pre-defined.  This routine only updates their states.
    start_epoch = 0
    if os.path.isfile(filename):
        print("=> loading checkpoint '{}'".format(filename))
        checkpoint = torch.load(filename,map_location='cpu')
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        print("=> loaded checkpoint '{}' (epoch {})"
                  .format(filename, checkpoint['epoch']))
    else:
        print("=> no checkpoint found at '{}'".format(filename))

    return model, optimizer

Network = CNNATM

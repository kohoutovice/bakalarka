# -*- coding: utf-8 -*-
'''
This Program stitches two images based on user input correspondences
Author: Tony Joseph
'''

import numpy as np
import cv2
import matplotlib.pyplot as plt
import sys
import os
import argparse





def getImageStitch(image1, image2, H):
    new_resolution = (int(image1.shape[1] + image2.shape[1] * 1.5), int(image1.shape[0]*1.5))
    stitch_image1 = cv2.warpPerspective(image2, H, new_resolution) / 100
    stitch_image2 = cv2.warpPerspective(image2, H, new_resolution) / 1
    stitch_image3 = cv2.warpPerspective(image2, H, new_resolution) / 2
    stitch_image1[0:image1.shape[0], 0:image1.shape[1]] += image1 / 1
    stitch_image2[0:image1.shape[0], 0:image1.shape[1]] += image1 / 100
    stitch_image3[0:image1.shape[0], 0:image1.shape[1]] += image1 / 2

    return (stitch_image1, stitch_image2, stitch_image3,)



image1 =  cv2.resize(cv2.imread('overview.png'),(3840,2160))/255
image2 =  cv2.resize(cv2.imread('e112.png'),(3840,2160))/255
print(image1.shape)
#image1 = cv2.resize(image1, (1000, 600))
#image2 = cv2.resize(image2, (1000, 600))




H = np.load('S1000001_img_3040.png_to_S1090001_img_6011.png.npy')
print(H)
stitched_images = getImageStitch(image1, image2, H)

####OVERVIEW
point1 = np.array([[[1370,1735]]],dtype='float32')

#####ENTRY
point2 = np.array([[[3025,650]]],dtype='float32')


pointsOut = cv2.perspectiveTransform(point2, H)
print(pointsOut)



cv2.namedWindow('e112')
cv2.namedWindow('overview')


cv2.circle(image1,(point1[0][0][0],point1[0][0][1]),10,(255,0,0),-1)
cv2.circle(image2,(point2[0][0][0],point2[0][0][1]),10,(255,0,0),-1)

cv2.circle(image1,(pointsOut[0][0][0],pointsOut[0][0][1]),10,(0,255,0),-1)
cv2.circle(image2,(point2[0][0][0],point2[0][0][1]),10,(0,255,0),-1)

def draw_circle(event,x,y,flags,param):
    if event == cv2.EVENT_LBUTTONDOWN:
        x = x*5
        y = y*5
        print(x,y)
        cv2.circle(image2,(x,y),10,(0,255,255),-1)

        pointsOut = cv2.perspectiveTransform(np.array([[[x,y]]],dtype='float32'), H)
        cv2.circle(image1,(pointsOut[0][0][0],pointsOut[0][0][1]),10,(0,255,255),-1)


cv2.setMouseCallback('e112',draw_circle)


while True:
    cv2.imshow('overview', cv2.resize(image1,(768,432)))
    cv2.imshow('e112', cv2.resize(image2,(768,432)))
    if cv2.waitKey(20) & 0xFF == 27:
        break
#cv2.waitKey(0)
exit(0)

stitched_images = [cv2.resize(img, (0, 0), fx=0.25, fy=0.25) for img in stitched_images]

i = 0
while True:
    cv2.imshow('output', stitched_images[i])
    if cv2.waitKey() == 27:
        break;
    i = (i + 1) % len(stitched_images)

cv2.imwrite('output.jpg', stitched_image)
# Display the Output Image:
cv2.namedWindow("Stitched_image")
cv2.moveWindow("Stitched_image", 300,250)
cv2.imshow("Stitched_image", stitched_image)
cv2.waitKey(0)

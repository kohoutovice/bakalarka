# -*- coding: utf-8 -*-
'''
This Program stitches two images based on user input correspondences
Author: Tony Joseph
'''

import numpy as np
import cv2
import matplotlib.pyplot as plt
import sys
import os
import argparse
from numpy.linalg import inv


parser = argparse.ArgumentParser(description='tf-pose-estimation run')
parser.add_argument('--overview', type=str)
parser.add_argument('--detail', type=str)
parser.add_argument('--do_hom', type=str, help="detail to overview homography")
parser.add_argument('--com', action="store_true")
args = parser.parse_args()

detail_cap = cv2.VideoCapture(args.detail)
overview_cap = cv2.VideoCapture(args.overview)

_, detail = detail_cap.read()
_, overview = overview_cap.read()

image1 =  overview/255
image2 =  detail/255


H = np.load(args.do_hom)


cv2.namedWindow('e112')
cv2.namedWindow('overview')


def draw_circle(event,x,y,flags,param):
    if event == cv2.EVENT_LBUTTONDOWN:
        x = x*5
        y = y*5
        print(x,y)
        cv2.circle(image1,(x,y),10,(0,255,255),-1)

        pointsOut = cv2.perspectiveTransform(np.array([[[x,y]]],dtype='float32'), H)

        cv2.circle(image2,(int(pointsOut[0][0][0]),int(pointsOut[0][0][1])),10,(0,255,255),-1)


cv2.setMouseCallback('overview',draw_circle)


while True:
    cv2.imshow('overview', cv2.resize(image1,(768,432)))
    cv2.imshow('e112', cv2.resize(image2,(768,432)))
    if cv2.waitKey(20) & 0xFF == 27:
        break
#cv2.waitKey(0)
exit(0)

import argparse
import logging
import sys
import time
import glob

from tf_pose import common
import cv2
import numpy as np
from tf_pose.estimator import TfPoseEstimator
from tf_pose.networks import get_graph_path, model_wh
import pickle
import numpy
from scipy.spatial import distance

logger = logging.getLogger('TfPoseEstimator')
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)


class Color(object):
    c = [
    [255,255,255],
    [255,0,255],
    [255,255,0],
    [255,0,0],
    [0,255,255],
    [0,0,255],
    [128,128,128],
    [128,0,128],
    [128,128,0],
    [128,0,0],
    [0,128,128],
    [0,0,128],
    ]
    """docstring for Color."""
    def __init__(self, arg):
        pass

persons = []
'''
persons = [
[{'frame':42,'pose':posedata}],

]
'''

all_poses = []
'''
all_poses = [
[{'id':id, 'pose':posedata},] #frame
]

'''

def new_person(frame,pose):
    persons.append([{"frame":frame,'pose':pose}])
    return len(persons)-1

def add_person_pose(id,frame,pose):
    persons[id].append({"frame":frame,'pose':pose})

image_w = 0
image_h = 0

def get_pairs(current_poses,last_poses):
    distances = []
    '''
    distances = [
    [(c_i,l_i,dist),(c_i,l_i,dist)]
    ]
    '''
    for i,current_pose in enumerate(current_poses):
        person_dist = []
        neck_current = (int(current_pose.body_part[1].x * image_w + 0.5), int(current_pose.body_part[1].y * image_h + 0.5))
        for j, last_pose in enumerate(last_poses):
            neck_last = (int(last_pose.body_part[1].x * image_w + 0.5), int(last_pose.body_part[1].y * image_h + 0.5))
            dist = numpy.linalg.norm(neck_current-neck_last)
            person_dist.append((i,j,dist))
        distances.append(person_dist)

    best_fit = {}
    for pose_dist in distances:
        p_index = None
        d_min = 99999999
        index_min = None
        for dist in pose_dist:
            if dist[2] < d_min:
                d_min = dist[2]
                p_index = dist[0]
                index_min = dist[1]

        best_fit[p_index] = (index_min,d_min)

    return best_fit

def get_pair_to_pose(current_pose,last_poses):

    neck_current = (int(current_pose.body_parts[1].x * image_w + 0.5), int(current_pose.body_parts[1].y * image_h + 0.5))
    min_dist = 9999999999999
    dist = min_dist
    poseData = None
    for lpose in last_poses:
        id = lpose['id']
        pose = lpose['pose']

        neck_last = (int(pose.body_parts[1].x * image_w + 0.5), int(pose.body_parts[1].y * image_h + 0.5))
        dist = dst = distance.euclidean(neck_current,neck_last)
        print(dist)
        if dist < min_dist:
            min_dist = dist
            poseData = lpose
    return dist, poseData


def process_frame(index,poses):
    current_frame_poses = []
    for pose in poses:
        if len(all_poses) == 0:
            id = new_person(index,pose)
            current_frame_poses.append({'id':id,'pose':pose})
        else:
            dist, poseData = get_pair_to_pose(pose,all_poses[-1])

            add_person_pose(poseData['id'],index,poseData['pose'])
            current_frame_poses.append(poseData)

    all_poses.append(current_frame_poses)




parser = argparse.ArgumentParser(description='tf-pose-estimation run here')
parser.add_argument('--video', type=str, default='')
parser.add_argument('--pose', type=str, default='')

parser.add_argument('--model', type=str, default='cmu', help='cmu / mobilenet_thin')
parser.add_argument('--file', type=str, default='out.pic', help='pickled file output')
parser.add_argument('--ui', type=bool, default=False, help='pickled file output')

parser.add_argument('--resize', type=str, default='432x368',
                    help='if provided, resize images before they are processed. default=0x0, Recommends : 432x368 or 656x368 or 1312x736 ')
parser.add_argument('--resize-out-ratio', type=float, default=4.0,
                    help='if provided, resize heatmaps before they are post-processed. default=1.0')

args = parser.parse_args()

w, h = model_wh(args.resize)


cap = cv2.VideoCapture(args.video)


if cap.isOpened() is False:
    print("Error opening video stream or file")

poses = pickle.load(open(args.pose,'rb'))
for i, pose in enumerate(poses):
    if cap.isOpened():
        ret_val, image = cap.read()
        if i < 1200:
            print(i)
            continue

        process_frame(i,pose)
        for pose_data in all_poses[-1]:
            id = pose_data['id']
            pose_k = pose_data['pose']

            # indexes
            # https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/output.md#keypoint-ordering
            image_h, image_w = image.shape[:2]
            body_part = pose_k.body_parts[1]
            color = Color.c[id]
            neck = (int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5))
            cv2.circle(image, neck, 3, color, thickness=3, lineType=8, shift=0)

            print("LEN",len(all_poses))


        print("============================================")
        print(all_poses:)
        print("============================================")
        if args.ui:
            image = cv2.resize(image, (640,360))                    # Resize image
            cv2.imshow('tf-pose-estimation result', image)
            cv2.waitKey(0)
            #if cv2.waitKey(1) & 0xFF == ord('q'):
            #    break

#!/bin/bash
cd $SCRATCHDIR || exit 1
module add tensorflow-1.7.1-gpu-python3
git clone https://gitlab.com/kohoutovice/bakalarka.git
cd bakalarka/gait/tf-openpose/
module add swig-3.0.8
cd tf_pose/pafprocess/
swig -python -c++ pafprocess.i && python3 setup.py build_ext --inplace
cd ../..

pip3 install opencv-python --user
pip3 install slidingwindow --user
cd models/graph/cmu/
bash download.sh
cd ../../..
module rm python-3.4.1-gcc python34-modules-gcc
module add python34-modules-intel



python3 run_eval_video.py --video /storage/brno8/home/xjurca08/BUT_pedestrians/BUT_pedestrians/ --resize 656x368 --file all.656x368.pose
#python3 run_eval_video.py --video S1090001.MP4 --resize 1312x736 --file S1090001.MP4.high.pose
#cp S1090001.MP4.high.pose /storage/brno2/home/xjurca08/

mkdir dataset
cd dataset
cp /storage/brno8/home/xjurca08/bbox_train.zip ./
cp /storage/brno8/home/xjurca08/bbox_test.zip ./
unzip bbox_test.zip
unzip bbox_train.zip
cd ..
python3.5 run_eval_dir.py --glob './dataset/*/*/*jpg' --file /storage/brno2/home/xjurca08/descs/mars/gait.mars.pic  --model cmu

#!/bin/bash

install_lib () {
    mkdir $1
    cd $1
    apt download $1
    ar -x *.deb
    unxz data.tar.xz
    tar -xvf data.tar
    LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$(pwd)/usr/lib/x86_64-linux-gnu/
    cd ..
}

install_bin () {
    mkdir $1
    cd $1
    apt download $1
    ar -x *.deb
    unxz data.tar.xz
    tar -xvf data.tar
    PATH=$PATH:$(pwd)/usr/bin
    cd ..
}

module add cuda-8.0
module load cudnn-5.1

module add cmake-3.2.3
module add opencv-3.3.1-py34
module add hdf5-1.10.3-intel
module add atlas-3.10.1-gcc4.7.0

cd $SCRATCHDIR || exit 1
git clone https://github.com/CMU-Perceptual-Computing-Lab/openpose.git
cd openpose/
mkdir build
cd build/


install_bin protobuf-compiler
install_lib libprotoc10
install_lib libprotobuf10
install_lib liblmdb-dev
install_lib libprotobuf-dev
install_lib libgoogle-glog-dev
install_lib libatlas-dev

/scratch.ssd/xjurca08/job_9448757.arien-pro.ics.muni.cz/openpose/build/protobuf/build
CMAKE_INCLUDE_PATH=$(pwd)/libatlas-dev/usr/include/:$(pwd)/protobuf/build/include/:$(pwd)/libgoogle-glog-dev/usr/include/:/software/atlas-3.10.1/gcc4.7.0/include/
export CMAKE_INCLUDE_PATH
CMAKE_LIBRARY_PATH=$(pwd)/libatlas-base-dev/usr/lib:$(pwd)/protobuf/build/lib/:$(pwd)/libgoogle-glog-dev/usr/lib/x86_64-linux-gnu
export CMAKE_LIBRARY_PATH
CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH:/software/hdf5/1.10.3/intel:$(pwd)/protobuf/build/lib/:/software/atlas-3.10.1/gcc4.7.0/lib/
export CMAKE_PREFIX_PATH
cmake ..  -DBUILD_PYTHON=True

make

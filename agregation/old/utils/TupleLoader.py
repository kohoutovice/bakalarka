import glob

class TupleLoader:
    """docstring for ToupleLoader."""
    _folder = ""
    _regex = ""
    def __init__(self, folder,regex):
        self._folder = folder
        self._regex = regex

    def fetch(self):
        return set(glob.glob(self._folder.rstrip("/") + "/" + self._regex))

        

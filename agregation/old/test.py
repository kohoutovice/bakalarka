from network import *
from siameseDataset import *
import torch
import torch.nn as nn
import torchvision
import torchvision.transforms as transforms
import torch.nn.functional as F
import torch
import torch.nn
import numpy as np
import datetime
from sklearn import metrics

from matplotlib import pyplot as plt
import matplotlib


def euclidean_distance(v1,v2):
    dist = torch.norm(v1 - v2)
    return dist

def agregate_mean(vecs):
    mean = vecs.mean(dim=-2)
    assert len(mean) == 128
    return mean

def agregate_center(vec):
    r =  vec[int(len(vec)/2)]
    assert len(r) == 128
    return r

def classIt(v1,v2,tresh = 0.3):
    dist = euclidean_distance(v1,v2)
    if dist.item() < tresh:
        return torch.from_numpy(np.array([1],dtype=np.float32))
    else:
        return torch.from_numpy(np.array([0],dtype=np.float32))

dataset = SiameseNetworkDataset("/home/rmin/Dokumenty/extracted/transposed.pic","/home/rmin/Dokumenty/extracted/same_indexes.pic","/home/rmin/Dokumenty/extracted/diff_indexes.pic")


net = NeuralNet().to(device)
if os.path.isfile('./network.torch'):
    print("Loading model form file")
    net.load_state_dict(torch.load("./network.torch"))








criterion = ContrastiveLoss()
optimizer = optim.RMSprop(net.parameters(),lr = 0.0005 )


counter = []
loss_history = []
iteration_number= 0
increment = 5000
start = 0
end = increment

for epoch in range(0,5):
    optimizer.zero_grad()
    first = []
    second = []
    labels = []
    for i in range(start,end):
        t0, t1 , label = dataset[i]
        needed_len = 30
        if len(t0.tolist()) < needed_len or len(t1.tolist()) < needed_len:
            continue
        first.append(t0.tolist()[:needed_len])
        second.append(t1.tolist()[:needed_len])
        labels.append(label.tolist())
        #if i% 1000 == 0:
        #print("Input vector",i)
    start = end
    end = end + increment
    if end >= len(dataset):
        start = 0
        end = increment

    t0 = torch.tensor(first)
    t1 = torch.tensor(second)
    labels = torch.tensor(labels)

    output1,output2 = net(t0.float(),t1.float())

    loss_contrastive = criterion(output1,output2,labels)
    loss_contrastive.backward()
    optimizer.step()

    print("Epoch number {}\n Current loss {}\n".format(epoch,loss_contrastive.item()))
    iteration_number += 1
    counter.append(iteration_number)
    loss_history.append(loss_contrastive.item())

torch.save(net.state_dict(), "network.torch")












net.eval()

correct_network = 0
correct_avg = 0
correct_center = 0

bad_network = 0
bad_avg = 0
bad_center = 0

avg_network = []
avg_avg = []
avg_center = []

labels_network = []


print(len(dataset))
l = 12000
for i in range(l):
    t0,t1,label = dataset[i]

    #Network
    first = []
    second = []
    labels = []
    first.append(t0.tolist())
    second.append(t1.tolist())

    t0f = torch.tensor(first)
    t1f = torch.tensor(second)
    labels = torch.tensor(labels)
    output1,output2 = net(t0f.float(),t1f.float())
    avg_network.append(euclidean_distance(output1,output2).item())
    labels_network.append(label.item())
    if classIt(output1,output2) == label:
        correct_network = correct_network + 1
    else:
        bad_network = bad_network + 1


    #avg
    output1 = agregate_mean(t0)
    output2 = agregate_mean(t1)
    avg_avg.append(euclidean_distance(output1,output2).item())
    if classIt(output1,output2) == label:
        correct_avg = correct_avg + 1
    else:
        bad_avg = bad_avg + 1

    #Center
    output1 = agregate_center(t0)
    output2 = agregate_center(t1)
    avg_center.append(euclidean_distance(output1,output2).item())
    if classIt(output1,output2) == label:
        correct_center = correct_center + 1
    else:
        bad_center = bad_center + 1

    if i%1000 == 0:
        print(i)
        print(datetime.datetime.now())
        print("correct_network",correct_network,"bad_network",bad_network)
        print("correct_avg",correct_avg,"bad_avg",bad_avg)
        print("correct_center",correct_center,"bad_center",bad_center)



fpr = dict()
tpr = dict()
print(len(labels_network))
print(len(avg_network))
print()
print(labels_network[0])
print(avg_network[0])


fpr["net"], tpr["net"], _ = metrics.roc_curve(np.array(labels_network),np.array(avg_network))
fpr["avg"], tpr["avg"], _ = metrics.roc_curve(np.array(labels_network),np.array(avg_avg))
fpr["center"], tpr["center"], _ = metrics.roc_curve(np.array(labels_network),np.array(avg_center))


plt.figure()
lw = 2
for key in fpr:
    plt.plot(fpr[key], tpr[key], lw=lw, label=key)

#plt.xlim([0.0, 1.0])
#plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver operating characteristic example')
plt.legend(loc="lower right")
plt.show()

print("Final agregated score:")
print("network",correct_network/l)
print("avg",correct_avg/l)
print("center",correct_center/l)

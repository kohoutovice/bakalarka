import cv2
import pickle
import numpy as np
import glob
import argparse
import os
import shutil
from matplotlib import pyplot as plt
import numpy as np
import cv2


def get_hip(pose):
    try:
        return pose[8]
    except Exception as e:
        pass
    try:
        return pose[11]
    except Exception as e:
        pass
    raise Exception("Nohip")

parser = argparse.ArgumentParser(description='')
parser.add_argument('--folder', action='store', default=None, help='folder')
args = parser.parse_args()

if args.folder:
    poses = glob.glob(args.folder.rstrip("/") + "/*.pose")
    poses.sort()

    height = 550
    width = 256
    n_channels = 4

    shift = np.array([128,130])
    scale = 150
    for pose_file in poses:
        pose = pickle.load(open(pose_file,'rb'))
        blank_image = np.zeros((height,width,n_channels), np.uint8)
        neck = np.array(pose[1])
        hip = np.array(get_hip(pose))
        body_norm = np.linalg.norm(neck-hip)
        points = []
        for point in pose:
            point = np.array(point)
            point = point*scale
            point = point + shift
            x, y = int(point[0]), int(point[1])
            print("======",x,y)
            points.append((x,y))
            cv2.circle(blank_image, (x,y), 3, (0,0,255,255), thickness=4)
        red_color = (255,0,0,255)
        cv2.line(blank_image,points[0],points[1],red_color,3)
        cv2.line(blank_image,points[0],points[14],red_color,3)
        cv2.line(blank_image,points[0],points[15],red_color,3)
        cv2.line(blank_image,points[16],points[14],red_color,3)
        cv2.line(blank_image,points[15],points[17],red_color,3)
        cv2.line(blank_image,points[2],points[1],red_color,3)
        cv2.line(blank_image,points[5],points[1],red_color,3)
        cv2.line(blank_image,points[2],points[3],red_color,3)
        cv2.line(blank_image,points[4],points[3],red_color,3)
        cv2.line(blank_image,points[5],points[6],red_color,3)
        cv2.line(blank_image,points[7],points[6],red_color,3)
        cv2.line(blank_image,points[1],points[8],red_color,3)
        cv2.line(blank_image,points[1],points[11],red_color,3)
        cv2.line(blank_image,points[12],points[11],red_color,3)
        cv2.line(blank_image,points[12],points[13],red_color,3)
        cv2.line(blank_image,points[9],points[8],red_color,3)
        cv2.line(blank_image,points[9],points[10],red_color,3)

        cv2.imshow("image",blank_image)
        cv2.imwrite(pose_file + ".png", blank_image)


        cv2.waitKey(10000)

import glob
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os, os.path


parser = argparse.ArgumentParser(description='')
parser.add_argument('--folder', action='store', default=None, help='folder')
parser.add_argument('--save', action='store', default=None, help='folder')
args = parser.parse_args()

x = []
for track in glob.glob(args.folder.rstrip("/") + "/*/*/*/*/"):
    print(track)
    x.append(int((len([name for name in os.listdir(track) if os.path.isfile(os.path.join(track, name))])/2)))

fig = plt.figure()

plt.hist(np.array(x),bins=200)
plt.xlabel('Délka stopy (počet snímků)')
plt.ylabel('Počet výskytů dané délky stopy')
plt.title("Histogram délek extrahovaných stop")
plt.show()

if args.save:
    fig.savefig(args.save, bbox_inches='tight')

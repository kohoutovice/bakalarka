PYTHONPATH=/home/rmin/Dropbox/skola/FIT/bakalarka/gait/tf-openpose/

python3.6 track_new.py --overview /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/overview.mp4 \
 --entry /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/entry.mp4 \
 --d105 /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/d105.mp4 \
 --e112 /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/e112.mp4 \
 --overview_pose /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/overview.mp4.pose \
 --entry_pose /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/entry.mp4.pose \
 --d105_pose /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/d105.mp4.pose \
 --e112_pose /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/e112.mp4.pose \
 --entry_homography /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/entry.npy \
 --d105_homography /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/d105.npy \
 --e112_homography /run/media/jjurca/17010F223A137D4C/dataset_sources/fit/e112.npy \
 --overview_tracks  /tmp/overview.tracks.pic \
 --entry_tracks  /tmp/entry.tracks.pic \
 --d105_tracks  /tmp/d105.tracks.pic \
 --e112_tracks  /tmp/e112.tracks.pic \
 --ui

 python3.6 track_new.py --overview /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/2.mp4 \
  --entry /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/1.mp4 \
  --d105 /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/3.mp4 \
  --e112 /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/4.mp4 \
  --overview_pose /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/2.mp4.pose \
  --entry_pose /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/1.mp4.pose \
  --d105_pose /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/3.mp4.pose \
  --e112_pose /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/4.mp4.pose \
  --entry_homography /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/2_to_1.npy \
  --d105_homography /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/2_to_3.npy \
  --e112_homography /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/2_to_4.npy \
  --overview_tracks  /tmp/2.tracks.pic \
  --entry_tracks  /tmp/1.tracks.pic \
  --d105_tracks  /tmp/3.tracks.pic \
  --e112_tracks  /tmp/4.tracks.pic \
  --ui


python3.6 pose_visual.py --folder /run/media/jjurca/17010F223A137D4C/BUT_pedestrians/fit_1688/entry/6400/

python3.6 create_homography.py --src /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/4.mp4 --dst /run/media/jjurca/17010F223A137D4C/dataset_sources/nove_sady/1.mp4

#TF openpose
python3.6 run_image.py --image ../../dataset/homography/d105.png

#GAIT
python3.6 train_triplets.py  -b 5000 \
--load /home/rmin/Dokumenty/extracted/gait_train/gait.but.train.triplets.last.75 \
--eval /home/rmin/Dokumenty/extracted/gait_train/gait.but.test.triplets.last.75 \
--model cnn \
--ui


#FUSION
python3.6 train.py \
--train /home/rmin/Dokumenty/extracted/fusion_train/BUT_pedestrians_fusion_train_pg.triplets \
--test /home/rmin/Dokumenty/extracted/fusion_train/BUT_pedestrians_fusion_test_pg.triplets \
--model pgsimple \
--batch 5000


#FACE
python3.6 validate.agregated.triplets.py --triplets /home/rmin/Dokumenty/extracted/BUT/BUT/BUT_pedestrians_face_descs_train.triplets  --label FACE

#PERSON
python3.6 validate.agregated.triplets.py --triplets /home/rmin/Dokumenty/extracted/BUT/BUT/BUT_pedestrians_person_descs_train.triplets  --label PERSON

#GAIT
python3.6 validate.agregated.triplets.py --triplets /home/rmin/Dokumenty/extracted/fusion_train/gait.but.train.descs.triplets.150 --label GAIT



#FPR grafy fuze
python3.6 validate.fpr.py --fpr /home/rmin/Dokumenty/extracted/fusion_nets/fusion.torch.2.simple.fpr --label "Fůze Simple"  --color green --style=0 --lw 2 --fpr /home/rmin/Dokumenty/extracted/fusion_nets/fusion.torch.2.cat.fpr --label "Fůze Concat" --color green --style=1 --lw 2 --fpr /home/rmin/Dokumenty/extracted/fusion_nets/fusion.torch.5.sum.fpr --label "Fůze Sum" --color green --style=3 --lw 2 --fpr /home/rmin/Dokumenty/extracted/BUT/BUT/BUT_pedestrians_person_descs_test.triplets.fpr --label "postava" --color blue --style 0 --lw 2 --fpr /home/rmin/Dokumenty/extracted/BUT/BUT/BUT_pedestrians_face_descs_train.triplets.fpr --label "obličej" --color blue --style 1 --lw 2 --fpr /home/rmin/Dokumenty/extracted/nets/gait.triplet.fpr.cnn.rnn.150 --label "chůze" --color blue --style 3 --lw 2 --title "Fůze (postava + obličej + chůze)"

#FPR grafy fuze postava + oblicej
python3.6 validate.fpr.py --fpr /home/rmin/Dokumenty/extracted/fusion_nets/fusion.torch.9.pg.simple.fpr --label "Fůze simple" --color green --style=0 --lw 1 --fpr /home/rmin/Dokumenty/extracted/fusion_nets/fusion.torch.2.pg.cat.fpr --label "Fůze concat" --color green --style=1 --lw 1 --fpr /home/rmin/Dokumenty/extracted/fusion_nets/fusion.torch.8.pg.sum.fpr --label "Fůze sum" --color green --style=3 --lw 1 --fpr /home/rmin/Dokumenty/extracted/BUT/BUT/BUT_pedestrians_person_descs_test.triplets.fpr --label "postava" --color blue --style 0 --lw 1 --fpr /home/rmin/Dokumenty/extracted/nets/gait.triplet.fpr.cnn.rnn.150 --label "chůze" --color blue --style 3 --lw 1 --title "Fůze (postava + chůze)"
